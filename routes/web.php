<?php

use App\Task; 
use App\Utente; 
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});

Route::get('users', function()
{
    return 'Users!';
});

Route::get('Prova',function()
{
    return 'Questa è una prova';

});
*/
Route::group(['middleware' => 'web'], function () {

	  Route::get('/', function () { 
		  $tasks = Task::orderBy('created_at', 'asc')->get();
		  return view('tasks', [ 'tasks' => $tasks ]);
		  //return view('tasks');
	  }); 
	  Route::post('/task', function (Request $request) { 
		  
		  $validator = Validator::make($request->all(), [ 'name' => 'required|max:255' ]); 
		  if ($validator->fails()) { 
			  return redirect('/') ->withInput() ->withErrors($validator); 
		  } 
		  $task = new Task; 
		  $task->name = $request->name; 
		  $task->save(); 
		  return redirect('/'); 
		  
	  }); 
      Route::delete('/task/{task}', function (Task $task) { 
	       $task->delete(); 
	       return redirect('/'); 
	  }); 

	  Route::get( '/hook', function(){
	  	file_get_contents('https://api.telegram.org/bot259883285:AAGa9BEMsP0U5AQphZznsQ2L8E-zIbTQMLs/sendMessage?chat_id=69009838&text=hook');
	  		echo exec('cd /var/www/laravel && git pull --no-edit');
	  		return "ok7";
	  });
	  Route::post('/addUser', function (Request $request) { 
			
		   switch($request->consenso){
			   case 'on':
			   		$request->consenso=1;
			   		break;
			   default:
			   		$request->consenso=0;
			   		break;
			   		
		   }	
		   switch($request->sponsor){
			   case 'on':
			   		$request->sponsor=1;
			   		break;
			   default:
			   		$request->sponsor=0;
			   		break;
			   		
		   }	
		   switch($request->clausola1c){
			   case 'on':
			   		$request->clausola1c=1;
			   		break;
			   default:
			   		$request->clausola1c=0;
			   		break;
			   		
		   }			  
		   
		   $validator = Validator::make($request->all(), [ 'nome' => 'required|max:30','cognome' => 'required|max:30','email' => 'required|email','telefono' => 'required|numeric|italian_phone','cap' => 'required|numeric|min:5' ]); 
			  if ($validator->fails()) { 
				  return redirect('/') ->withInput() ->withErrors($validator); 
			  } 

		   
	           $utente = new Utente; 
		   $utente->nome = $request->nome; 
		   $utente->cognome = $request->cognome; 
		   $utente->email = $request->email; 
		   $utente->telefono = $request->telefono;
		   $utente->cap = $request->cap;  
		   $utente->data_nascita = $request->data_nascita; 
		   $utente->provenienza = $request->provenienza;
		   $utente->consenso = $request->consenso;
		   $utente->sponsor = $request->sponsor;
		   $utente->clausola1c = $request->clausola1c;
		   $utente->user_agent=$request->header('User-Agent'); 
		   
		   
		   $utente->save(); 
	       return redirect('/'); 
	  }); 
});
