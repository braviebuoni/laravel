<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUtenteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('utente', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email',80)->unique();
            $table->string('telefono',15);
            $table->string('nome',40);
            $table->string('cognome',40);
            $table->date('data_nascita');
            $table->integer('cap')->unsigned();
            $table->string('provenienza',40);
            $table->tinyInteger('consenso')->unsigned();
            $table->tinyInteger('sponsor')->unsigned();
            $table->tinyInteger('clausola1c')->unsigned();
            $table->string('user_agent',300);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('utente');
    }
}
