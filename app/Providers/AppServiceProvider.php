<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('italian_phone', function($attribute, $value, $parameters) {
	        
	        $prefix_array = array("330","331","333","334","335","336","337","338","339","360","366","368","385","340","342","344","345","346","347","348","349","320","324","327","328","329","380","388","389","391","392","393","377","373");
			$prefix_ok=false;
	        
	        $fake_sequence=false;
	        for($i=0;$i<=9;$i++){
		        if(stripos($value, $i.$i.$i.$i.$i)!==false){
			        $fake_sequence=true;
		        }
	        }
	        
	        foreach ($prefix_array as &$prefix) {
				if($prefix==substr($value, 0, 3)){
					$prefix_ok=true;
					break;
				}
			}
	        
            return strlen($value)>=9&&strlen($value)<11&&$prefix_ok&&!$fake_sequence;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
