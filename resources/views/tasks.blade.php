@extends('app') 
@section('content') 
<!-- Bootstrap Boilerplate... --> 
<div class="panel-body"> <!-- Display Validation Errors -->
@include('errors.general') 
<!-- New Task Form --> 
<form action="{{ url('task') }}" method="POST" class="form-horizontal"> 
	{!! csrf_field() !!} 
	<!-- Task Name --> 
	<div class="form-group"> 
		<label for="task" class="col-sm-3 control-label">Task1</label> 
		<div class="col-sm-6"> <input type="text" name="name" id="task-name" class="form-control"> </div> 
	</div> 
	<!-- Add Task Button --> 
	<div class="form-group"> 
		<div class="col-sm-offset-3 col-sm-6"> 
			<button type="submit" class="btn btn-default"> <i class="fa fa-plus"></i> Add Task </button> 
		</div>
	</div>
</form> 
</div> 

<!-- Current Tasks --> 
@if (count($tasks) > 0) 
<div class="panel panel-default"> 
	<div class="panel-heading"> Current Tasks </div> 
	<div class="panel-body"> 
		<table class="table table-striped task-table"> 
			<!-- Table Headings --> 
			<thead> 
				<th>Task</th> 
				<th>&nbsp;</th> 
			</thead> 
			<!-- Table Body --> 
			<tbody> @foreach ($tasks as $task) 
				<tr> 
					<!-- Task Name --> 
					<td class="table-text">
						<div>{{ $task->name }}</div> 
					</td>
					<td> 
						<form action="{{ url('task/'.$task->id) }}" method="POST"> {!! csrf_field() !!} {!! method_field('DELETE') !!} <button>Delete Task</button> </form> 
					</td> 
				</tr> @endforeach 
			</tbody>
		</table> 
	</div> 
</div> 
@endif

<!-- New Utente Form --> 
<form action="{{ url('addUser') }}" method="POST" class="form-horizontal"> 
	{!! csrf_field() !!} 
	<!-- Task Name --> 
	<div class="form-group"> 
		<label for="task" class="col-sm-3 control-label">Nome</label> 
		<div class="col-sm-6"> <input type="text" name="nome" id="utente-nome" class="form-control"> </div> 
	</div> 
	<div class="form-group"> 
		<label for="task" class="col-sm-3 control-label">Cognome</label> 
		<div class="col-sm-6"> <input type="text" name="cognome" id="utente-cognome" class="form-control"> </div> 
	</div> 
	<div class="form-group"> 
		<label for="task" class="col-sm-3 control-label">Email</label> 
		<div class="col-sm-6"> <input type="text" name="email" id="utente-email" class="form-control"> </div> 
	</div> 
	<div class="form-group"> 
		<label for="task" class="col-sm-3 control-label">Telefono</label> 
		<div class="col-sm-6"> <input type="text" name="telefono" id="utente-telefono" class="form-control"> </div> 
	</div> 
	<div class="form-group"> 
		<label for="task" class="col-sm-3 control-label">Cap</label> 
		<div class="col-sm-6"> <input type="text" name="cap" id="utente-cap" class="form-control"> </div> 
	</div> 
	<div class="form-group"> 
		<label for="task" class="col-sm-3 control-label">Data Nascita</label> 
		<div class="col-sm-6"> <input type="text" name="data_nascita" id="utente-data_nascita" class="form-control"> </div> 
	</div> 
	<div class="form-group"> 
		<label for="task" class="col-sm-3 control-label">Provenienza</label> 
		<div class="col-sm-6"> <input type="text" name="provenienza" id="utente-provenienza" class="form-control"> </div> 
	</div> 
	<div class="form-group"> 
		<label for="task" class="col-sm-3 control-label">Consenso</label> 
		<div class="col-sm-6"> <input type="checkbox" name="consenso" id="utente-consenso" class="form-control"> </div> 
	</div> 
	<div class="form-group"> 
		<label for="task" class="col-sm-3 control-label">Sponsor</label> 
		<div class="col-sm-6"> <input type="checkbox" name="sponsor" id="utente-sponsor" class="form-control"> </div> 
	</div> 
	<div class="form-group"> 
		<label for="task" class="col-sm-3 control-label">Clausola 1c</label> 
		<div class="col-sm-6"> <input type="checkbox" name="clausola1c" id="clausola1c" class="form-control"> </div> 
	</div> 
	<!-- Add Task Button --> 
	<div class="form-group"> 
		<div class="col-sm-offset-3 col-sm-6"> 
			<button type="submit" class="btn btn-default"> <i class="fa fa-plus"></i> Aggiungi Utente </button> 
		</div>
	</div>
</form> 
</div> 



@endsection
